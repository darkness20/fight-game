import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter, ...args) {
  const heightBlockRight = document.getElementById("right-fighter-indicator");
  const heightBlockLeft = document.getElementById("left-fighter-indicator");
  let arrChars = [];
  let keyTimesFirst = [];
  let newKeyTimesFirst = [];
  let timeFirst = [];
  let keyTimesSecond = [];
  let newKeyTimesSecond = [];
  let timeSecond = [];

  document.addEventListener('keydown', function(event) {
    if (event.repeat) return;
    arrChars.push(event.code);
  });

  document.addEventListener('keyup', function(event) {
    console.log("arrChars",arrChars);
    if (arrChars.length == 0) return;
    let runFunc = true;
    for (let arg of args) {
        if (!arrChars.includes(arg)) {
            runFunc = false;
            break;
        }
    }
    if (runFunc) {
      
      controls.PlayerOneCriticalHitCombination.forEach((value, index) => {
        if(keyTimesFirst[index] != undefined) {
          newKeyTimesFirst[index] = new Date();
          timeFirst[index] = newKeyTimesFirst[index] - keyTimesFirst[index];
        }

        if(arrChars.includes(value) && keyTimesFirst[index] == undefined) {
          keyTimesFirst[index] = new Date();
          console.log(keyTimesFirst);
        }

        console.log("TimeFirst", timeFirst);

        if(timeFirst[2] > 10000 && timeFirst.length == 3 || newKeyTimesFirst.length == 0 && keyTimesFirst.length == 3) {
          let criticalHit = firstFighter.attack * 2;
          heightBlockRight.style.width = heightBlockRight.offsetWidth - criticalHit + "px";
          timeFirst.length = 0;
          if(newKeyTimesFirst.length != 0) {
            keyTimesFirst.length = 0;
          }
        }
        debugger;
      });

      controls.PlayerTwoCriticalHitCombination.forEach((value, index) => {
        console.log(arrChars[value]);
        if(keyTimesSecond[index] != undefined) {
          newKeyTimesSecond[index] = new Date();
          timeSecond[index] = newKeyTimesSecond[index] - keyTimesSecond[index];
        }

        if(arrChars.includes(value) && keyTimesSecond[index] == undefined) {
          keyTimesSecond[index] = new Date();
          console.log(keyTimesSecond);
        }

        console.log("TimeSecond", timeSecond);

        if(timeSecond[2] > 10000 && timeSecond.length == 3 || newKeyTimesSecond.length == 0 && keyTimesSecond.length == 3) {
          let criticalHit = secondFighter.attack * 2;
          heightBlockLeft.style.width = heightBlockLeft.offsetWidth - criticalHit + "px";
          timeSecond.length = 0;
          if(newKeyTimesSecond.length != 0) {
            keyTimesSecond.length = 0;
          }
        }
      });

      if(arrChars.includes(controls.PlayerOneAttack)) {
        let loss = getDamage(firstFighter, secondFighter, arrChars);
        heightBlockRight.style.width = heightBlockRight.offsetWidth - loss + "px";

      } else if(arrChars.includes(controls.PlayerTwoAttack)) {
        let loss = getDamage(secondFighter, firstFighter, arrChars);
        heightBlockLeft.style.width = heightBlockLeft.offsetWidth - loss + "px";
      }


    }
    
    if(heightBlockRight.offsetWidth == 1) {
      heightBlockRight.style.width = 0 + "px";
      return new Promise((resolve) => {
        resolve(firstFighter);
      });
    }
    if(heightBlockLeft.offsetWidth == 1) {
      heightBlockLeft.style.width = 0 + "px";
      return new Promise((resolve) => {
        resolve(secondFighter);
      });
    }
  

    arrChars.length = 0;
  });

}

export function getDamage(attacker, defender, arrChars) {
  let hitPower = getHitPower(attacker);
  let damage = 0;
  let blockPower = getBlockPower(defender);
  let key = false;
  if(arrChars.includes(controls.PlayerOneAttack) && arrChars.includes(controls.PlayerTwoBlock) ||
    arrChars.includes(controls.PlayerTwoAttack) && arrChars.includes(controls.PlayerOneBlock)) {

    if(blockPower < hitPower) {
      damage = hitPower - blockPower;
    } else {
      damage = 0;
    }

  } else if (arrChars.includes(controls.PlayerOneAttack) || arrChars.includes(controls.PlayerTwoAttack)) {
    damage = hitPower;
  }

  return damage;
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random() * (2 - 1) + 1;
  let power = fighter.attack * criticalHitChance * 100 / 100;

  return power;
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random() * (2 - 1) + 1;
  let power = fighter.defense * dodgeChance;

  return power;
}
