import { createElement } from '../helpers/domHelper';
import { fighters } from '../helpers/mockData';
import { createFightersSelector } from './fighterSelector';

export function createFighters(fighters) {
  const selectFighter = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));


  fightersList.append(...fighterElements);
  container.append(preview, fightersList);
  console.log(fighterElements[0]);

  return container;
}

function createFighter(fighter, selectFighter) {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter'});
  fighterElement.setAttribute("id", fighter._id);
  const imageElement = createImage(fighter);
  const onClick = (event) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}
